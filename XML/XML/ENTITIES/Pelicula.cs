﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using XML.DAL;

namespace XML.ENTITIES
{
    class Pelicula
    {
        public string codigo { get; set; }
        public string titulo { get; set; }
        public string director { get; set; }
        public Interprete interprete { get; set; }
        public string genero { get; set; }


        public override string ToString()
        {
            return "" + codigo + " " + titulo + " " + director + " " + interprete + " " + genero;
        }
    }
}
