﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XML.DAL;
using XML.ENTITIES;

namespace XML
{
    public partial class FrmXml : Form
    {
        Pelicula pelicula;

        public FrmXml()
        {
            InitializeComponent();
            CenterToScreen();
            mostrar();
        }

        public void mostrar()
        {
            dgvPeliculas.Rows.Clear();
            foreach (Pelicula p in new PeliculaDAL().LeerXml())
            {
                dgvPeliculas.Rows.Add(p, p.codigo, p.titulo, p.director, p.interprete.nombre, p.interprete.nacionalidad, p.interprete.annoNacimiento, p.genero);
            }
        }

        public void limpiar()
        {
            txtCodigo.ResetText();
            txtTitulo.ResetText();
            txtDirector.ResetText();
            txtNombre.ResetText();
            txtNacionalidad.ResetText();
            txtAnnoNacimiento.ResetText();
            txtGenero.ResetText();
            txtCodigo.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Pelicula p = new Pelicula();
                p.codigo = txtCodigo.Text.Trim();
                p.titulo = txtTitulo.Text.Trim();
                p.director = txtDirector.Text.Trim();
                Interprete i = new Interprete();
                i.nombre = txtNombre.Text.Trim();
                i.nacionalidad = txtNacionalidad.Text.Trim();
                i.annoNacimiento = txtAnnoNacimiento.Text.Trim();
                p.interprete = i;
                p.genero = txtGenero.Text.Trim();
                new PeliculaDAL().AgregarDatos(p);
                mostrar();
                limpiar();
                MessageBox.Show("Pelicula registrada");
            }
            catch (Exception)
            {
                MessageBox.Show("Intente nuevamente");
            }  
        }

        private void btnCrearXML_Click(object sender, EventArgs e)
        {
            PeliculaDAL xml = new PeliculaDAL();
            xml.CrearXML("Gestion_Peliculas");
            mostrar();
            MessageBox.Show("Archivo XML creado", "Informacion");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (pelicula != null)
                {
                    pelicula.titulo = txtTitulo.Text.Trim();
                    pelicula.director = txtDirector.Text.Trim();
                    Interprete i = new Interprete();
                    i.nombre = txtNombre.Text.Trim();
                    i.nacionalidad = txtNacionalidad.Text.Trim();
                    i.annoNacimiento = txtAnnoNacimiento.Text.Trim();
                    pelicula.interprete = i;
                    pelicula.genero = txtGenero.Text.Trim();
                    new PeliculaDAL().ModificarXml(pelicula);
                    mostrar();
                    limpiar();
                    MessageBox.Show("Pelicula Modificada");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Intente nuevamente");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (pelicula != null)
            {
                if (MessageBox.Show("Desea eliminar la pelicula: " + pelicula.titulo, "Informacion", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    new PeliculaDAL().EliminarXml(pelicula.codigo);
                    mostrar();
                    limpiar();
                }
                else
                {
                    return;
                }
            }
        }

        private void dgvPeliculas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtCodigo.Enabled = false;
                pelicula = (Pelicula)dgvPeliculas.Rows[e.RowIndex].Cells[0].Value;

                if (pelicula != null)
                {
                    txtCodigo.Text = pelicula.codigo;
                    txtTitulo.Text = pelicula.titulo;
                    txtDirector.Text = pelicula.director;
                    txtNombre.Text = pelicula.interprete.nombre;
                    txtNacionalidad.Text = pelicula.interprete.nacionalidad;
                    txtAnnoNacimiento.Text = pelicula.interprete.annoNacimiento;
                    txtGenero.Text = pelicula.genero;
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
