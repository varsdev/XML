﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using XML.ENTITIES;

namespace XML.DAL
{
    class PeliculaDAL
    {
        XmlDocument doc;
        string rutaXml = "Datos.xml";

        public void CrearXML(string nodoRaiz)
        {
            doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "no");

            XmlNode nodo = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, nodo);

            XmlNode elemento = doc.CreateElement(nodoRaiz);
            doc.AppendChild(elemento);

            doc.Save(rutaXml);
        }

        public void AgregarDatos(Pelicula p)
        {
            doc = new XmlDocument();
            doc.Load(rutaXml);

            XmlNode pelicula = CrearPelicula(p);

            XmlNode nodo = doc.DocumentElement;

            nodo.InsertAfter(pelicula, nodo.LastChild);

            doc.Save(rutaXml);
        }

        public XmlNode CrearPelicula(Pelicula p)
        {
            XmlNode pelicula = doc.CreateElement("pelicula");

            XmlElement xid = doc.CreateElement("codigo");
            xid.InnerText = p.codigo;
            pelicula.AppendChild(xid);

            XmlElement xtitulo = doc.CreateElement("titulo");
            xtitulo.InnerText = p.titulo;
            pelicula.AppendChild(xtitulo);

            XmlElement xdirector = doc.CreateElement("director");
            xdirector.InnerText = p.director;
            pelicula.AppendChild(xdirector);
            //
            XmlNode interprete = doc.CreateElement("interprete");
   
            XmlElement xnombre = doc.CreateElement("nombre");
            xnombre.InnerText = p.interprete.nombre;
            interprete.AppendChild(xnombre);

            XmlElement xnacionalidad = doc.CreateElement("nacionalidad");
            xnacionalidad.InnerText = p.interprete.nacionalidad;
            interprete.AppendChild(xnacionalidad);

            XmlElement xannonaci = doc.CreateElement("año_nacimiento");
            xannonaci.InnerText = p.interprete.annoNacimiento;
            interprete.AppendChild(xannonaci);

            pelicula.AppendChild(interprete);
            //
            XmlElement xgenero = doc.CreateElement("genero");
            xgenero.InnerText = p.genero;
            pelicula.AppendChild(xgenero);

            return pelicula;
        }

        public List<Pelicula> LeerXml()
        {
            doc = new XmlDocument();
            doc.Load(rutaXml);

            XmlNodeList listaPeliculas = doc.SelectNodes("Gestion_Peliculas/pelicula");
            List<Pelicula> pelis = new List<Pelicula>();

            foreach (XmlNode item in listaPeliculas)
            {
                Pelicula pel = new Pelicula();
                pel.codigo = item.SelectSingleNode("codigo").InnerText;
                pel.titulo = item.SelectSingleNode("titulo").InnerText;
                pel.director = item.SelectSingleNode("director").InnerText;
                XmlNodeList listaInterpretes = item.SelectNodes("interprete");
                foreach (XmlNode a in listaInterpretes)
                {
                    Interprete inter = new Interprete();
                    inter.nombre = a.SelectSingleNode("nombre").InnerText;
                    inter.nacionalidad = a.SelectSingleNode("nacionalidad").InnerText;
                    inter.annoNacimiento = a.SelectSingleNode("año_nacimiento").InnerText;
                    pel.interprete = inter;
                }
                pel.genero = item.SelectSingleNode("genero").InnerText;
                pelis.Add(pel);
            }
            return pelis;
        }

        public void ModificarXml(Pelicula p)
        {
            doc = new XmlDocument();
            doc.Load(rutaXml);

            XmlNodeList listaPeliculas = doc.SelectNodes("Gestion_Peliculas/pelicula");
            
            foreach (XmlNode item in listaPeliculas)
            {
                if (item.FirstChild.InnerText == p.codigo)
                {
                    item.SelectSingleNode("titulo").InnerText = p.titulo;
                    item.SelectSingleNode("director").InnerText = p.director;
                    //XmlNodeList listaInterpretes = doc.SelectNodes("Gestion_Peliculas/pelicula/interprete");
                    XmlNodeList listaInterpretes = item.SelectNodes("interprete");
                    foreach (XmlNode a in listaInterpretes)
                    {
                        a.SelectSingleNode("nombre").InnerText = p.interprete.nombre;
                        a.SelectSingleNode("nacionalidad").InnerText = p.interprete.nacionalidad;
                        a.SelectSingleNode("año_nacimiento").InnerText = p.interprete.annoNacimiento;                 
                    }
                    item.SelectSingleNode("genero").InnerText = p.genero;
                }
            }

            doc.Save(rutaXml);
        }

        public void EliminarXml(string codigo)
        {
            doc = new XmlDocument();
            doc.Load(rutaXml);

            XmlNodeList listaPeliculas = doc.SelectNodes("Gestion_Peliculas/pelicula");

            XmlNode borra = doc.DocumentElement;

            foreach (XmlNode item in listaPeliculas)
            {
                if (item.SelectSingleNode("codigo").InnerText == codigo)
                {
                    XmlNode borrar = item;
                    borra.RemoveChild(borrar);
                }
            }

            doc.Save(rutaXml);
        }




    }
}
