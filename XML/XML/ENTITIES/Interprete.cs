﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace XML.DAL
{
    class Interprete
    {
        public string nombre { get; set; }
        public string nacionalidad { get; set; }
        public string annoNacimiento { get; set; }

        public override string ToString()
        {
            return ""+ nombre + " " + nacionalidad + " " + annoNacimiento;
        }
    }
}
